#!

#########################################################################
##                                                                     ##
##     header_detection.sh                                             ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################


# header_detection.sh ${gz} $affected_patients $unaffected_patients $header_other $header_line_limit "headerDetection_log.txt"
vcf_gz=$1
affected_patients="$2"
unaffected_patients="$3"
header_other="$4"
header_line_limit="$5"
log="$6"



# vcf_gz="/mnt/share/Users/Gael/Documents/Git_projects/08002_bourgeron/dataset/Dyslexia.gatk-vqsr.splitted.norm.vep.merged_first_10000.vcf.gz"
# affected_patients="C0011JZ C0011K2 C0011K3 C0011KA IP00FNP IP00FNW IP00FNY"
# unaffected_patients="C0011JY C0011K1 C0011K5 C0011KB"
# header_other="CHROM POS ID REF ALT QUAL FILTER INFO FORMAT"
# header_line_limit=1000
# log="headerDetection_log.txt"


# header elements
TEMPO="$header_other $unaffected_patients $affected_patients" # Warning: works only with space separators
echo -e "\n\nTHE DECLARED FIELD ARE:\n $TEMPO" > ${log}
IFS=' ' read -r -a TEMPO_ARRAY <<< "$TEMPO" # split according to space
LENGTH=$(echo ${#TEMPO_ARRAY[@]})
echo -e "\n\nTHE NUMBER OF DECLARED FIELD ARE: $LENGTH" >> ${log}
# end header elements

# line number containing the header
header_line_nb=$(zcat ${vcf_gz} | awk -v tempo="$TEMPO" -v length2="$LENGTH" -v limit="$header_line_limit" '
    BEGIN{
        split(tempo, array1, " ")
    }{
        if ($0 ~ array1[length2 - 1]){
            field_nb = 0 ; 
            for (key in array1) {
                if ($0 ~ array1[key]){field_nb++}
            }
            if (field_nb == length2){
                OFS=""
                ORS=""
                print NR
                exit # stop the awk parsing
            }else{
                print "\n\nERROR IN AWK PROCESS: MISSING FIELDS IN THE HEADER. CHECK THE affected_patients, unaffected_patients AND header_other PARAMETERS\n\n"
                exit # stop the awk parsing
            }
        }
        if (NR == limit){
            print "\n\nERROR IN AWK PROCESS: HEADER STILL NOT FOUND AFTER SCANNING " limit " OF LINES\n\n"
            exit 1 # stop the awk parsing
        }
    }
')
re="^[0-9]+$"
if [[ ! $header_line_nb =~ $re ]] ; then
    echo "ERROR: header_line_nb NOT A NUMBER IN THE headerDetection PROCESS" >&2 # sent the value in the stderr
    echo $header_line_nb >&2 # sent the value in the stderr
    # exit 1
else
    echo -e "\n\nAFTER AWK PROCESS: THE HEADER IS LINE NUMBER ${header_line_nb}"  >> ${log}
fi
# end line number containing the header

echo -n ${header_line_nb} >&1 # sent the value in the stdout, -n to remove the EOF at the end of the value



