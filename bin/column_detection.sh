#!

#########################################################################
##                                                                     ##
##     column_detection.sh                                             ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################


# column_detection.sh ${vcf_gz} ${header_line_nb} "${patient}" "columnDetection_log.txt"
vcf_gz=$1
header_line_nb="$2"
patients="$3"
log="$4"

# column number of first group
zcat ${vcf_gz} | awk -v var1="$patients" -v var2="$header_line_nb" '
    BEGIN{
        split(var1, array1, " ")
    }{
        if (NR == var2){
            
            ORS=" "
            for(i=1;i<=NF;i++){
                for(key in array1){
                    if ($i ~ array1[key]){
                        print i
                    }
                }
            }
            exit # stop the awk parsing
        }else{
            next
        }
    }
' > tempo.txt
patients_col_nb=$(cat tempo.txt | sed 's/[[:blank:]]*$//')
echo -e "\n\nTHE COLUMNS CORRESPONDING TO\n${patients}\nARE\n${patients_col_nb}" > ${log} # to deal with the .sh running several times
# end column number of first group

echo ${patients_col_nb} >&1 # sent the value in the stdout





