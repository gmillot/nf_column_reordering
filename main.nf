nextflow.enable.dsl=1
/*
#########################################################################
##                                                                     ##
##     main.nf                                                         ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################
*/


//////// Arguments of nextflow run

params.modules = ""

//////// end Arguments of nextflow run


//////// Variables

config_file = file("${projectDir}/nextflow.config")
log_file = file("${launchDir}/.nextflow.log")
modules = params.modules // remove the dot -> can be used in bash scripts

//////// end Variables


//////// Variables from config.file that need to be checked

in_path_test = file("${in_path}/${sample_name}") // to test if exist below
vcf_ch_test = file("${in_path}/${sample_name}") // to test if exist below

//////// end Variables from config.file that need to be checked


//////// Channels

//// used more than once

// vcf_ch = Channel.fromPath("${in_path}/${sample_name}", checkIfExists: false) // I could use true, but I prefer to perform the check below, in order to have a more explicit error message
Channel.fromPath("${in_path}/${sample_name}", checkIfExists: false).into{vcf_ch1 ; vcf_ch2 ; vcf_ch3} // or vcf_ch = Channel.fromPath("${in_path}/${sample_name}") if only one channel. I could use true, but I prefer to perform the check below, in order to have a more explicit error message

//// end used more than once

//// used once

affected_patients_ch = Channel.value("${affected_patients}")
unaffected_patients_ch = Channel.value("${unaffected_patients}")
header_other_ch = Channel.value("${header_other}")
header_line_limit_ch = Channel.value("${header_line_limit}")
patient_ch = Channel.value(["${affected_patients}", "${unaffected_patients}"]) // create two entries, see https://www.nextflow.io/docs/latest/channel.html#from
patient_name_ch = Channel.value(["affected_patients", "unaffected_patients"]) // create two entries, see https://www.nextflow.io/docs/latest/channel.html#from

//// end used once

//////// end Channels



//////// Checks

if(system_exec == 'local' || system_exec == 'slurm'){
    def file_exists1 = in_path_test.exists()
    if( ! file_exists1){
        error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID in_path_test PARAMETER IN nextflow.config FILE: ${in_path_test}\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
    }
    def file_exists2 = log_file.exists()
    if( ! file_exists2){
        error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID annot1_path PARAMETER IN nextflow.config FILE: ${annot1_path}\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
    }
}else{
    error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID system_exec PARAMETER IN nextflow.config FILE: ${system_exec}\n\n========\n\n"
}


//////// end Checks



//////// Processes


process WorkflowVersion { // create a file with the workflow version in out_path
    label 'bash' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}", mode: 'copy'
    cache 'false'

    output:
    file "Run_info.txt"

    script:
    """
    echo "Project (empty means no .git folder where the main.nf file is present): " \$(git -C ${projectDir} remote -v | head -n 1) > Run_info.txt # works only if the main script run is located in a directory that has a .git folder, i.e., that is connected to a distant repo
    echo "Git info (empty means no .git folder where the main.nf file is present): " \$(git -C ${projectDir} describe --abbrev=10 --dirty --always --tags) >> Run_info.txt # idem. Provide the small commit number of the script and nextflow.config used in the execution
    echo "Cmd line: ${workflow.commandLine}" >> Run_info.txt
    echo "execution mode": ${system_exec} >> Run_info.txt
    modules=$modules # this is just to deal with variable interpretation during the creation of the .command.sh file by nextflow. See also \$modules below
    if [[ ! -z \$modules ]] ; then
        echo "loaded modules (according to specification by the user thanks to the --modules argument of main.nf)": ${modules} >> Run_info.txt
    fi
    echo "Manifest's pipeline version: ${workflow.manifest.version}" >> Run_info.txt
    echo "result path: ${out_path}" >> Run_info.txt
    echo "nextflow version: ${nextflow.version}" >> Run_info.txt
    echo -e "\\n\\nIMPLICIT VARIABLES:\\n\\nlaunchDir (directory where the workflow is run): ${launchDir}\\nprojectDir (directory where the main.nf script is located): ${projectDir}\\nworkDir (directory where tasks temporary files are created): ${workDir}" >> Run_info.txt
    echo -e "\\n\\nUSER VARIABLES:\\n\\nout_path: ${out_path}\\nin_path: ${in_path}" >> Run_info.txt
    """
}
//${projectDir} nextflow variable
//${workflow.commandLine} nextflow variable
//${workflow.manifest.version} nextflow variable
//Note that variables like ${out_path} are interpreted in the script block



process headerDetection {
    label 'bash' // see the withLabel: bash in the nextflow config file 
    publishDir path: "${out_path}", mode: 'copy', overwrite: false
    cache 'false'

    //no channel input here for the vcf, because I do not transform it
    input:
    file vcf_gz from vcf_ch1
    val affected_patients from affected_patients_ch
    val unaffected_patients from unaffected_patients_ch
    val header_other from header_other_ch
    val header_line_limit from header_line_limit_ch
    // see the scope for the use of affected_patients which is already a variable from .config file

    output:
    stdout header_line_nb_ch // channel that can be used any time because a single value
    file "headerDetection_log.txt"
    // vcf_gz not sent into channel because value will not be transformed in this current process
    // file "headerDetection_log.txt" : this must be written to have the file printed into the result file. Nextflow undestands that it corresponds to the log file inside the header_detection.sh script because headerDetection_log.txt is also written as argument in the script block
    // But we can replace file "log" if all the ${log} are replaced by log in the header_detection.sh file

    script:
    """
    header_detection.sh ${vcf_gz} "${affected_patients}" "${unaffected_patients}" "${header_other}" ${header_line_limit} "headerDetection_log.txt"
    """
    // write between "" (example "$unaffected_patients") to make a single argument when the variable is made of several values separated by a space. Otherwise, several arguments will be considered
    // warning: ${in_path}/${sample_name} instead of ${vcf_gz} does not work, because the file used is not here anymore (do not use path in nextflow)
}


process columnDetection {
    label 'bash' // see the withLabel: bash in the nextflow config file 
    //publishDir path: "${out_path}", mode: 'copy', overwrite: false
    cache 'false'

    //no channel input here for the vcf, because I do not transform it
    input:
    file vcf_gz from vcf_ch2
    val header_line_nb from header_line_nb_ch
    each patient from patient_ch

    output:
    //stdout into patient_col_nb_ch // this does not work for two reasons: first, only one of the "each" input is sent into the channel. Second, 
    file "columnDetection_log.txt" into tempo_ch1
    stdout into patient_col_nb_ch

    script:
    """
    column_detection.sh ${vcf_gz} ${header_line_nb} "${patient}" "columnDetection_log.txt"
    """
}
tempo_ch1.collectFile(name:"columnDetection_log.txt").subscribe{
    f ->
    def file="${out_path}"
    return [file, f.copyTo(file)]
}
// subscribe{ f -> f.copyTo("${out_path}") } does not work, I had to use https://github.com/nextflow-io/nextflow/issues/1812

patient_col_nb_ch.collectFile(name:"col_position.txt").subscribe{ f -> f.copyTo("${out_path}") }
//https://bioinformatics.stackexchange.com/questions/15722/nextflow-can-i-parameterize-similar-processes-inside-a-for-loop



process Backup {
    label 'bash' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}", mode: 'copy', overwrite: false // since I am in mode copy, all the output files will be copied into the publishDir. See \\wsl$\Ubuntu-20.04\home\gael\work\aa\a0e9a739acae026fb205bc3fc21f9b
    cache 'false'

    input:
    file config_file
    file log_file

    output:
    file "${config_file}" // warning message if we use file config_file
    file "${log_file}" // warning message if we use file log_file
    file "Log_info.txt"

    script:
    """
    echo -e "full .nextflow.log is in: ${launchDir}\nThe one in the result folder is not complete (miss the end)" > Log_info.txt
    """
}


//////// end Processes
